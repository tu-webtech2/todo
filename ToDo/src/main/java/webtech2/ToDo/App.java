package webtech2.ToDo;




import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import webtech2.ToDo.entities.DBNote;
import webtech2.ToDo.entities.DBNote_;
import webtech2.ToDo.entities.DBUser;
import webtech2.ToDo.entities.DBUser_;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
		final App app = new App();

		app.init();
		app.createData();
		app.queryData();
		//DBNote note = app.entityManager.find(DBNote.class, 4L);
		//note.setTitle("g to the g lets hope this works");
		//System.out.println(app.entityManager.find(DBNote.class, 4L).getTitle());
		app.shutdown();        
    }
    
    private EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory;
	private final static String PERSISTENCE_UNIT = "webtech2.ToDo";


	private void init() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
		this.entityManager = this.entityManagerFactory.createEntityManager();
		this.entityManager.getTransaction().begin();
	}

	private void createData() {
		
		final DBUser user = new DBUser();
		user.setName("jeezus");
		user.setPassword("thepony");
		this.entityManager.persist(user);
		
		final DBUser user2 = new DBUser();
		user2.setName("anaconda");
		user2.setPassword("thepony2");
		//user2.setNote(note);
		this.entityManager.persist(user2);
		
		final DBUser user3 = new DBUser();
		user3.setName("rababa");
		user3.setPassword("thepony3");
	
		this.entityManager.persist(user3);
		
		final DBNote note = new DBNote();
		note.setTitle("Wichtige Anmerkung");
		note.setText("Sehr sehr wichtig");
		note.setUser(user);
		note.setUser(user3);
		this.entityManager.persist(note);

	}

	private void queryData() {
		final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<DBNote> query = builder.createQuery(DBNote.class);
		final Root<DBNote> from = query.from(DBNote.class);

		final Path<DBUser> note = from.get(DBNote_.user);

		final Predicate predicate = builder.equal(note.get(DBUser_.name), "rababa");
		//final Order order = builder.asc(from.get(DBNote_.title));

		query.select(from).where(predicate);

		final List<DBNote> results = this.entityManager.createQuery(query).getResultList();
		for (DBNote dbUser : results) {
			System.out.println("OUTPUT: " + dbUser.getId());
		}

	}

	private void shutdown() {
		this.entityManager.flush();
		this.entityManager.getTransaction().commit();
		this.entityManager.close();
		this.entityManagerFactory.close();
	}
}
