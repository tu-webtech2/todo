package webtech2.ToDo.auth.permission;

import org.apache.shiro.authz.Permission;

public class ReadNoteItemPermission implements Permission {

	@Override
	public boolean implies(Permission p) {

		if (p instanceof FirstNoteItemPermission) {
			final FirstNoteItemPermission fnip = (FirstNoteItemPermission) p;
			if (fnip.check()) {
				return true;
			}
		}

		return false;
//		return true;
	}
}
