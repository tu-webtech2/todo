package webtech2.ToDo.auth.permission;

import org.apache.shiro.authz.Permission;

public class FirstNoteItemPermission implements Permission {

	private final long id;

	public FirstNoteItemPermission(final long id) {
		this.id = id;
	}
	@Override
	public boolean implies(Permission p) {
		return false;
	}

	public boolean check() {
		return this.id == 1;
	}
}
