package webtech2.ToDo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.Date;

import webtech2.ToDo.entities.*;

@Singleton
@Startup
public class StartupBean {

	@PersistenceContext
	private EntityManager entityManager;

	@PostConstruct
	public void startup() {

		final DBNote firstNoteItem = this.entityManager.find(DBNote.class, 1L);

		// only initialize once
		if (firstNoteItem == null) {

		
			final DBUser user = new DBUser();
			user.setName("jeezus");
			user.setPassword("thepony");
			this.entityManager.persist(user);
			
			final DBUser user2 = new DBUser();
			user2.setName("anaconda");
			user2.setPassword("thepony2");
			//user2.setNote(note);
			this.entityManager.persist(user2);
			
			final DBUser user3 = new DBUser();
			user3.setName("rababa");
			user3.setPassword("thepony3");
		
			this.entityManager.persist(user3);
			
			final DBNote note = new DBNote();
			note.setTitle("Wichtige Anmerkung");
			note.setText("Sehr sehr wichtig");
			note.setUser(user);
			note.setUser(user3);
			this.entityManager.persist(note);
		}
	}

	@PreDestroy
	public void shutdown() {
		// potential cleanup work
	}
}
