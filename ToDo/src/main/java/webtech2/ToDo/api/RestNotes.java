package webtech2.ToDo.api;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import webtech2.ToDo.entities.DBNote;
import webtech2.ToDo.entities.DBNote_;
import java.util.List;

@Path("/notes")
@Transactional
public class RestNotes {
	@PersistenceContext
	private EntityManager entityManager;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<DBNote> readAllAsJSON() {
		final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<DBNote> query = builder.createQuery(DBNote.class);

		final Root<DBNote> from = query.from(DBNote.class);

		query.select(from);

		return this.entityManager.createQuery(query).getResultList();
	}
	

	@Path("/get/{id}")
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public DBNote readAsJSON(@PathParam("id") final long id) {
		return this.entityManager.find(DBNote.class, id);
	}
	
	@Path("/list_all")
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public List<DBNote> listAll() {
		final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<DBNote> query = builder.createQuery(DBNote.class);

		final Root<DBNote> from = query.from(DBNote.class);

		query.select(from);

		return this.entityManager.createQuery(query).getResultList();
	}
	
	@Path("/list_unassigned")
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public List<DBNote> listUnassigned() {
		final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<DBNote> query = builder.createQuery(DBNote.class);

		final Root<DBNote> from = query.from(DBNote.class);
		final Predicate predicate = builder.isNull(from.get(DBNote_.user));

		query.select(from).where(predicate);

		return this.entityManager.createQuery(query).getResultList();
	}
	
	@Path("/list_user/{id}")
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public List<DBNote> listUser(@PathParam("id") final long id) {
		final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<DBNote> query = builder.createQuery(DBNote.class);

		final Root<DBNote> from = query.from(DBNote.class);
		final Predicate predicate = builder.equal(from.get(DBNote_.user), id);

		query.select(from).where(predicate);

		return this.entityManager.createQuery(query).getResultList();
	}
	
	@POST
	@Path("/add")
	@Consumes({MediaType.APPLICATION_JSON})
	public void addNote(DBNote noteToAdd) {
	  DBNote note = new DBNote();
	  note.setTitle(noteToAdd.getTitle());
	  note.setText(noteToAdd.getText());
	  note.setFinished(noteToAdd.isFinished());
	  this.entityManager.persist(note);
	}
	
	@PUT
	@Path("/del/{id}")
	public void deleteNote(@PathParam("id")final long id) {
		this.entityManager.remove(entityManager.find(DBNote.class, id));
	}
	
	@PUT
	@Path("/edit/{id}")
	@Consumes({MediaType.APPLICATION_JSON})
	public void editUser(@PathParam("id")final long id, DBNote changedNote) {
		DBNote note = entityManager.find(DBNote.class, id);
		note.setTitle(changedNote.getTitle());
		note.setText(changedNote.getText());
		note.setFinished(changedNote.isFinished());
	}

// XML brauchen wir nicht, oder?
/*	@Path("/{id}")
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_XML)
	public DBNote readAsXML(@PathParam("id") final long id) {
		return this.entityManager.find(DBNote.class, id);
	}*/
	
	
}
