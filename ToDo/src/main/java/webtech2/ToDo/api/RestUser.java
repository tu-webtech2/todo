package webtech2.ToDo.api;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import webtech2.ToDo.entities.DBUser;

import java.util.List;

@Path("/users")
@Transactional
public class RestUser {
	@PersistenceContext
	private EntityManager entityManager;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<DBUser> readAllAsJSON() {
		final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<DBUser> query = builder.createQuery(DBUser.class);

		final Root<DBUser> from = query.from(DBUser.class);

		query.select(from);

		return this.entityManager.createQuery(query).getResultList();
	}
	
	@Path("/list")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<DBUser> listAll() {
		final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<DBUser> query = builder.createQuery(DBUser.class);

		final Root<DBUser> from = query.from(DBUser.class);

		query.select(from);

		return this.entityManager.createQuery(query).getResultList();
	}

	@Path("/get/{id}")
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public DBUser readAsJSON(@PathParam("id") final long id) {
		return this.entityManager.find(DBUser.class, id);
	}
	
	@POST
	@Path("/register")
	@Consumes({MediaType.APPLICATION_JSON})
	public void registerUser(DBUser userToRegister) {
	  DBUser user = new DBUser();
	  user.setName(userToRegister.getName());
	  user.setPassword(userToRegister.getPassword());
	  user.setType(userToRegister.getType());
	  this.entityManager.persist(user);
	}
	
	@PUT
	@Path("/del/{id}")
	public void deleteUser(@PathParam("id")final long id) {
		this.entityManager.remove(entityManager.find(DBUser.class, id));
	}
	
	@PUT
	@Path("/edit/{id}")
	@Consumes({MediaType.APPLICATION_JSON})
	public void editUser(@PathParam("id")final long id, DBUser userToEdit) {
		DBUser user = entityManager.find(DBUser.class, id);
		user.setName(userToEdit.getName());
		user.setPassword(userToEdit.getPassword());
		user.setType(userToEdit.getType());
	}
	
	

// XML brauchen wir nicht, oder?
/*	@Path("/{id}")
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_XML)
	public DBUser readAsXML(@PathParam("id") final long id) {
		return this.entityManager.find(DBUser.class, id);
	}*/
}
