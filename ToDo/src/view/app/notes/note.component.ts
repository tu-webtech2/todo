import {Component} from '@angular/core';

@Component({
    selector: 'note'
})
export class NoteComponent {
    id: number;
    name: string;
    text: string;
    userID: number;
    done: boolean;

    constructor(id: number, name: string, text: string, userID: number, done: boolean) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.userID = userID;
        this.done = done;
    }
}