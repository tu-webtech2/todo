import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {NoteService} from './note.service';

@Component({
    selector: 'note-center',
    directives: [ROUTER_DIRECTIVES],
    providers: [NoteService],
    templateUrl: './app/templates/note-center.template.html'
})
export class NoteCenterComponent{}