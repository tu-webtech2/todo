import {Component, OnInit} from '@angular/core';
import {Router, ROUTER_DIRECTIVES} from '@angular/router';
import {NoteService} from './note.service';
import {NoteComponent} from "./note.component";

@Component({
    selector: 'dashboard',
    directives: [ROUTER_DIRECTIVES],
    templateUrl: './app/templates/note-center.template.html'
})
export class DashboardComponent implements OnInit {
    notes: NoteComponent[] = [];

    constructor(private router: Router, private noteService: NoteService) {}

    ngOnInit() {
        this.getNotes();
    }

    getNotes() {
        this.noteService.getNotes().then(notes => this.notes = notes);
    }

    showDetails(note: NoteComponent) {
        let link = ['/notes', note.id];
        this.router.navigate(link);
    }

    addNote() {this.router.navigate(['/notes/add']);}
}