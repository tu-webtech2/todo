import {Injectable} from '@angular/core';
import {NOTES} from './mock-notes';

@Injectable()
export class NoteService {
    getNotes() {
        return Promise.resolve(NOTES);
    }
    getNote(id: number) {
        return this.getNotes().then(notes => notes.filter(note => note.id === id)[0]);
    }
}