import {Component} from '@angular/core';
import {NoteComponent} from './note.component';

export var NOTES: NoteComponent[] = [
    {id: 1, name: 'Testnotiz', text: 'NOTIZ TEXT', userID: 1, done: false},
    {id: 2, name: 'asdad', text: 'NOTIZ TEXT', userID: 3, done: true},
    {id: 3, name: 'yxcyxc', text: 'NOTIZ TEXT', userID: 2, done: true},
    {id: 4, name: 'Testyxcxynotiz', text: 'NOTIZ TEXT', userID: 4, done: false},
    {id: 5, name: '243243asasd', text: 'NOTIZ TEXT', userID: 2, done: false},
    {id: 6, name: '213dscxcvv', text: 'NOTIZ TEXT', userID: 1, done: true},
    {id: 7, name: 'bgfjuzk fdd', text: 'NOTIZ TEXT', userID: 3, done: true},
    {id: 8, name: '123dsvf  efwefewfewf', text: 'NOTIZ TEXT', userID: 4, done: false},
];