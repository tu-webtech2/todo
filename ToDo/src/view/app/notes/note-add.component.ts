import {Component} from '@angular/core';
import {NoteComponent} from './note.component';
import {NoteService} from './note.service';

@Component({
    selector: 'note-add',
    templateUrl: './app/templates/note-add.template.html',
})
export class NoteAddComponent {
    note: NoteComponent;

    constructor(private noteService: NoteService) {}

    addNote() {
        //this.note = new NoteComponent();
    }

    goBack() {window.history.back();}
}