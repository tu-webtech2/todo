import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NoteComponent} from './note.component';
import {NoteService} from './note.service';
import {UserService} from "../users/user.service";
import {UserComponent} from "../users/user.component";

@Component({
    selector: 'note-detail',
    templateUrl: './app/templates/note-detail.template.html'
})
export class NoteDetailComponent implements OnInit, OnDestroy {
    note: NoteComponent;
    users: UserComponent[];
    sub: any;

    constructor(private noteService: NoteService, private userService: UserService, private route: ActivatedRoute) {}

    ngOnInit() {
        // + to convert string to number
        this.sub = this.route.params.subscribe(params =>
            {
                let id = +params['id'];
                this.noteService.getNote(id).then(note => this.note = note);
            });
        this.userService.getUsers().map(users => this.users = users);
    }

    ngOnDestroy() {this.sub.unsubscribe();}

    editNote() {
        // call rest

        // redirect
        window.history.back();
    }

    deleteNote() {
        // call rest Service to delete this.note
        // redirect
        window.history.back();
    }

    goBack() {window.history.back();}
}