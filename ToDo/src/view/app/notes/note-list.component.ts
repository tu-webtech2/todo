import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NoteComponent} from './note.component';
import {NoteService} from './note.service';
import {UserService} from "../users/user.service";
import {UserComponent} from "../users/user.component";

@Component({
    selector: 'note-list',
    templateUrl: './app/templates/note-list.template.html',
})
export class NoteListComponent implements OnInit{
    notes: NoteComponent[] = [];
    users: UserComponent[] =[];

    constructor(private router: Router, private noteService: NoteService, private userService: UserService) {}

    ngOnInit() {
        this.getNotes();
        this.getUsers();
    }

    getNotes() {
        this.noteService.getNotes().then(notes => this.notes = notes);
    }

    getUsers() {
        this.userService.getUsers().map(users => this.users = users);
    }

    showDetails(note: NoteComponent) {
        let link = ['/notes', note.id];
        this.router.navigate(link);
    }

    getName(note: NoteComponent) {
        //return this.users[note.userID - 1].name;
    }

    addNote() {this.router.navigate(['/notes/add']);}
}