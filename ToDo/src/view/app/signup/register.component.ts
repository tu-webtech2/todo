import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';

@Component({
    selector: 'login',
    directives: [ROUTER_DIRECTIVES],
    templateUrl: './app/templates/register.template.html'
})

export class RegisterComponent {}