import {provideRouter, RouterConfig}  from "@angular/router";
import {LoginComponent} from "./signup/login.component";
import {RegisterComponent} from "./signup/register.component";
import {NoteCenterComponent} from "./notes/note-center.component";
import {NoteDetailComponent} from "./notes/note-detail.component";
import {NoteAddComponent} from "./notes/note-add.component";
import {NoteListComponent} from "./notes/note-list.component";
import {UserCenterComponent} from "./users/user-center.component";
import {UserListComponent} from "./users/user-list.component";
import {UserDetailComponent} from "./users/user-detail.component";
import {UserAddComponent} from "./users/user-add.component";

export const routes: RouterConfig = [
    {path: '', redirectTo: '/login', pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'users', component: UserCenterComponent, children: [
        {path: '', component: UserListComponent},
        {path: 'add', component: UserAddComponent},
        {path: ':id', component: UserDetailComponent}
    ]},
    {path: 'notes', component: NoteCenterComponent, children: [
        {path: '', component: NoteListComponent},
        {path: 'add', component: NoteAddComponent},
        {path: ':id', component: NoteDetailComponent}
    ]}
];

export const APP_ROUTER_PROVIDERS = [provideRouter(routes)];