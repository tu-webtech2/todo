import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {UserService} from "./user.service";

@Component({
    selector: 'user-center',
    directives: [ROUTER_DIRECTIVES],
    providers: [UserService],
    templateUrl: './app/templates/user-center.template.html'
})
export class UserCenterComponent{}