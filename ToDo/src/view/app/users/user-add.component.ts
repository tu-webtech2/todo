import {Component} from '@angular/core';
import {UserService} from "./user.service";
import {UserComponent} from "./user.component";

@Component({
    selector: 'user-add',
    templateUrl: './app/templates/user-add.template.html',
})
export class UserAddComponent {
    user: UserComponent;

    constructor(private userService: UserService) {}

    addNote() {
        //this.note = new NoteComponent();
    }

    goBack() {window.history.back();}
}