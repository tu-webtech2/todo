import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserComponent} from './user.component';
import {UserService} from "./user.service";

@Component({
    selector: 'user-detail',
    templateUrl: './app/templates/user-detail.template.html',
})
export class UserDetailComponent implements OnInit, OnDestroy {
    user: UserComponent;
    sub: any;

    constructor(private userService: UserService, private route: ActivatedRoute) {}

    ngOnInit() {
        // + to convert string to number
        this.sub = this.route.params.subscribe(params =>
        {
            let id = +params['id'];
            this.userService.getUser(id).map(user => this.user = user);
        });
    }

    ngOnDestroy() {this.sub.unsubscribe();}

    editUser() {
        // call rest

        // redirect
        window.history.back();
    }

    deleteUser() {
        // call rest service to delete this.user
        //redirect
        window.history.back();
    }

    goBack() {window.history.back();}
}