import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserComponent} from './user.component';
import {UserService} from "./user.service";

@Component({
    selector: 'user-list',
    templateUrl: './app/templates/user-list.template.html'
})

export class UserListComponent implements OnInit {
    users: UserComponent[] = [];
    errorMessage: any;

    constructor(private router: Router, private userService: UserService) {}

    ngOnInit() {
        this.getUsers();
    }

    getUsers() {
        //this.userService.getUsers().then(users => this.users = users);
        this.userService.getUsers().subscribe(users => this.users = users, error => this.errorMessage = <any>error);
    }

    showDetails(id: number) {
        this.router.navigate(['/users', id])
    }

    addUser() {this.router.navigate(['/users/add']);}
}