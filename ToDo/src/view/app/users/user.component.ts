import {Component} from '@angular/core';

@Component({
    selector: 'user'
})
export class UserComponent {
    id: number;
    name: string;
    admin: boolean;

    constructor(id: number, name: string, admin: boolean) {
        this.id = id;
        this.name = name;
        this.admin = admin;
    }
}