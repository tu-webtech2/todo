import {Component} from '@angular/core';
import {UserComponent} from './user.component';

export var USERS: UserComponent[] = [
    {id: 1, name: 'SuperUser', admin: true},
    {id: 2, name: 'Ramadan', admin: false},
    {id: 3, name: 'Stefan', admin: false},
    {id: 4, name: 'Phillip', admin: false}
];