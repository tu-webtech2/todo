import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {USERS} from './mock-users';
import {UserComponent} from "./user.component";
import {Observable} from "rxjs/Observable";

@Injectable()
export class UserService {
    private usersUrl = 'app/users.json';

    constructor(private http: Http) {}

    getUsers(): Observable<UserComponent[]> {
        return this.http.get(this.usersUrl)
            .map(this.extractData)
            .catch(this.handleError);
        //return this.http.get(this.usersUrl).toPromise().then(response => response.json().data).catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        console.log(body);
        return body.data || { };
    }

    /*
    getUsers() {
        return Promise.resolve(USERS);
    }
    */

    getUser(id: number) {
        return this.getUsers().map(users => users.find(user => user.id === id));
    }

    private handleError (error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}