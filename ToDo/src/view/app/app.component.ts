import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from "@angular/router";
import {NoteService} from "./notes/note.service";
import {UserService} from "./users/user.service";
import "./rxjs-operators";
//import "rxjs/Rx";

@Component({
    selector: 'angular-app',
    templateUrl: './app/templates/app.template.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [NoteService, UserService]
})

export class AppComponent {
    isLoggedIn: boolean;

    constructor() {
        this.isLoggedIn = true;
    }
}