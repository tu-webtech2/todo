import { Injectable} from '@angular/core';
import { Headers, Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { UserComponent } from './users/user.component';
import { NoteComponent } from './notes/note.component';

@Injectable()
export class RestService{

	private usersUrl = "app/users";
	private notesUrl = "app/notes";

	constructor(private http: Http) { }
	
	//notes
	getNotes(): Promise<NoteComponent[]>{
		return this.http.get(this.notesUrl)
			.toPromise()
			.then(response => response.json().data)
			.catch(this.handleError)
	}
	
	private postNote(note: NoteComponent): Promise<NoteComponent> {
		let headers = new Headers({
			'Content-Type' : 'application/json'});
			return this.http
						.post(this.usersUrl, JSON.stringify(note), {headers: headers})
						.toPromise()
						.then(res => res.json().data)
						.catch(this.handleError);
	}
	
	
		// Update existing Note
	private putNote(note: NoteComponent) {
	  let headers = new Headers();
	  headers.append('Content-Type', 'application/json');

	  let url = `${this.notesUrl}/${note.id}`;

	  return this.http
				 .put(url, JSON.stringify(note), {headers: headers})
				 .toPromise()
				 .then(() => note)
				 .catch(this.handleError);
	}
	
	deleteNote(note: NoteComponent) {
	  let headers = new Headers();
	  headers.append('Content-Type', 'application/json');

	  let url = `${this.notesUrl}/${note.id}`;

	  return this.http
				 .delete(url, headers)
				 .toPromise()
				 .catch(this.handleError);
	}
	
	saveNote(note: NoteComponent): Promise<NoteComponent>  {
	  if (note.id) {
		return this.putNote(note);
	  }
	  return this.postNote(note);
	}
	
	//User
	getUsers():Promise<NoteComponent[]>{
		return this.http.get(this.usersUrl)
			.toPromise()
			.then(response => response.json().data)
			.catch(this.handleError)
	}
	
	private postUser(user: UserComponent): Promise<UserComponent> {
		let headers = new Headers({
			'Content-Type' : 'application/json'});
			return this.http
						.post(this.usersUrl, JSON.stringify(user), {headers: headers})
						.toPromise()
						.then(res => res.json().data)
						.catch(this.handleError);
	}
	
	private handleError(error : any){
		console.error('An error occured', error);
		return Promise.reject(error.message || error);
	}
	
		// Update existing User
	private putUser(user: UserComponent) {
	  let headers = new Headers();
	  headers.append('Content-Type', 'application/json');

	  let url = `${this.usersUrl}/${user.id}`;

	  return this.http
				 .put(url, JSON.stringify(user), {headers: headers})
				 .toPromise()
				 .then(() => user)
				 .catch(this.handleError);
	}
	
	deleteUser(user: UserComponent) {
	  let headers = new Headers();
	  headers.append('Content-Type', 'application/json');

	  let url = `${this.usersUrl}/${user.id}`;

	  return this.http
				 .delete(url, headers)
				 .toPromise()
				 .catch(this.handleError);
	}
	
	saveUser(user: UserComponent): Promise<UserComponent>  {
	  if (user.id) {
		return this.putUser(user);
	  }
	  return this.postUser(user);
	}
}